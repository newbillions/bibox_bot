#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import sched, time
import random
from bibox_api import get_market_depth,post_order

MAX_RETRY = 3

def core_business_logic(s, API_KEY,API_SECRET, SLEEP_TIME, MARKET, IS_BUY, current_token_amount, TOTAL_TOKEN_AMOUNT, BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG):
    if DEBUG: print("[DEBUG]entering core_business_logic\n[DEBUG]SLEEP_TIME:{}, MARKET:{}, IS_BUY:{}, current_token_amount:{}, TOTAL_TOKEN_AMOUNT:{}, BUY_SELL_WALL_PRICE:{},TOKEN_RAND_UPPER_LIMIT:{} ,TOKEN_RAND_LOWER_LIMIT:{}".format(SLEEP_TIME, MARKET, IS_BUY, current_token_amount, TOTAL_TOKEN_AMOUNT, BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT))
    print("[Progress] {}/{} Token for {}\n".format(current_token_amount,TOTAL_TOKEN_AMOUNT,MARKET))

    # Check current_token_amount < TOTAL_TOKEN_AMOUNT
    if current_token_amount >= TOTAL_TOKEN_AMOUNT:
        print("[INFO] current_token_amount:{} >= TOTAL_TOKEN_AMOUNT:{} exit the program".format(current_token_amount,TOTAL_TOKEN_AMOUNT))
        return

    # Try to get market_accept_price
    retry_count = 0
    while (retry_count < MAX_RETRY):
        retry_count += 1
        try:
            if IS_BUY:
                order_book = get_market_depth(MARKET)["bids"]
                order_book.sort(key=lambda x: x['price'],reverse=True)
            else:
                order_book = get_market_depth(MARKET)["asks"]
                order_book.sort(key=lambda x: x['price'])
            break
        except:
            if DEBUG: print("[ERROR]retry get_order_book "+str(retry_count))
            continue
    if retry_count >= MAX_RETRY:
        s.enter(SLEEP_TIME, 1, core_business_logic, (s,API_KEY,API_SECRET, SLEEP_TIME, MARKET, IS_BUY, current_token_amount, TOTAL_TOKEN_AMOUNT, BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG))
        print("[ERROR] fail to get_order_book after {} trails. sleep for {} and wait for next time".format(MAX_RETRY,SLEEP_TIME))
        return

    market_accept_price = float(order_book[0]['price'])
    print("\t[INFO]market_accept_price:%.10f\n" % (market_accept_price))

    # Test the BUY_SELL_WALL_PRICE condition
    if IS_BUY:
        if market_accept_price > BUY_SELL_WALL_PRICE:
            print("[INFO] BUY {} is GREATER than {}.sleep for {} and wait for next time".format(market_accept_price,BUY_SELL_WALL_PRICE,SLEEP_TIME))
            s.enter(SLEEP_TIME, 1, core_business_logic, (s,API_KEY,API_SECRET, SLEEP_TIME, MARKET, IS_BUY, current_token_amount, TOTAL_TOKEN_AMOUNT, BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG))
            return
    else:
        if market_accept_price < BUY_SELL_WALL_PRICE:
            print("[INFO] SELL {} is SMALLER than {}.sleep for {} and wait for next time".format(market_accept_price,BUY_SELL_WALL_PRICE,SLEEP_TIME))
            s.enter(SLEEP_TIME, 1, core_business_logic, (s,API_KEY,API_SECRET, SLEEP_TIME, MARKET, IS_BUY, current_token_amount, TOTAL_TOKEN_AMOUNT, BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG))
            return

    retry_count = 0
    while (retry_count < MAX_RETRY):
        retry_count += 1
        try:
            m_token_amount = random.randint(TOKEN_RAND_LOWER_LIMIT,TOKEN_RAND_UPPER_LIMIT)

            # Config cmd
            account_type = 0 #账户类型，0-普通账户，1-信用账户
            order_type = 2 #交易类型，1-市价单，2-限价单
            if IS_BUY: order_side = 1 # 1 买
            else: order_side = 2 # 2 卖
            pair = MARKET #交易对, BIX_BTC, BIX_ETH, ...
            pay_bix = 0 #是否bix抵扣手续费，0-不抵扣，1-抵扣
            price = market_accept_price
            amount = m_token_amount #委托数量
            money = market_accept_price * m_token_amount #委托金额

            cmds = [
                {
                    'cmd':"orderpending/trade",
                    'body':{
                        'pair':pair,
                        'account_type':account_type,
                        'order_type':order_type,
                        'order_side':order_side,
                        'pay_bix':pay_bix,
                        'price':price,
                        'amount':amount,
                        'money':money
                    }
                }
            ]

            for x in range(NUMBER_OF_ORDERS_PER_EXEC):
                
                # Check current_token_amount < TOTAL_TOKEN_AMOUNT
                if current_token_amount >= TOTAL_TOKEN_AMOUNT:
                    print("[INFO] current_token_amount:{} >= TOTAL_TOKEN_AMOUNT:{} exit the program".format(current_token_amount,TOTAL_TOKEN_AMOUNT))
                    return

                order = post_order(API_KEY,API_SECRET,cmds)
                current_token_amount += m_token_amount
                s_price = "%.10f" % price
                print("\t{} Order placed. price {} amount {}\n".format(x+1,s_price,amount))
            break
        except Exception as e:
            if DEBUG: print("[DEBUG] retry create_order {}count. {}".format(str(retry_count),e))
            if retry_count == MAX_RETRY: print("[ERROR]fail to create order after {} times. give up. {}".format(retry_count,e))
            continue

    s.enter(SLEEP_TIME, 1, core_business_logic, (s,API_KEY,API_SECRET, SLEEP_TIME, MARKET, IS_BUY, current_token_amount, TOTAL_TOKEN_AMOUNT, BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG))

def bibox_order(API_KEY,API_SECRET,SLEEP_TIME,MARKET,IS_BUY,TOTAL_TOKEN_AMOUNT,BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG):
    s = sched.scheduler(time.time, time.sleep)
    s.enter(1, 1, core_business_logic, (s,API_KEY,API_SECRET,SLEEP_TIME,MARKET,IS_BUY, 0, TOTAL_TOKEN_AMOUNT,BUY_SELL_WALL_PRICE,TOKEN_RAND_UPPER_LIMIT,TOKEN_RAND_LOWER_LIMIT,IS_MARKET_ORDER,NUMBER_OF_ORDERS_PER_EXEC,DEBUG))
    s.run()
