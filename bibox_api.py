import hmac
import hashlib
import json, requests

def getSign(data,secret):
    result = hmac.new(secret.encode("utf-8"), data.encode("utf-8"), hashlib.md5).hexdigest()
    return result

def doApiRequestWithApikey(url, cmds, api_key, api_secret):
    s_cmds = json.dumps(cmds)
    sign = getSign(s_cmds,api_secret)
    r = requests.post(url, data={'cmds': s_cmds, 'apikey': api_key,'sign':sign})
    return r.text

def post_order(api_key,api_secret,cmds):
    url = "https://api.bibox.com/v1/orderpending"
    return doApiRequestWithApikey(url,cmds,api_key,api_secret)

def get_market_depth(pair):
    url = "https://api.bibox.com/v1/mdata?cmd=depth&pair={}&size=10".format(pair)
    resp = requests.get(url=url)
    data = json.loads(resp.text)
    return data["result"]
